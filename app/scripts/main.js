'use strict';

$(document).ready(function(){
  var firstSectionSelector = '.homepage-wrapper';
  var slider = $('.home-slider').slick({
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    autoplay: false,
  });
  $('.story-slider').slick({
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 5000,
    pauseOnHover: false,
    });
  $('.nav-icon').on( 'click', function() {
    $('.nav-icon').toggleClass('open');
    $('nav').toggleClass('hide');
  });
  var mediaQuery = window.matchMedia('(max-width: 767px)');
  if (mediaQuery.matches) {
    $('.nav-icon').on('click', function() {
      $('.nav-wrapper').toggleClass('open-nav');
    });
    $('.nav-links a').on('click', function() {
      $('.nav-icon').trigger('click');
    });
    $('.partners-grid-item ul').slick({
      centerMode: false,
      arrows: false,
      variableWidth: true,
      swipeToSlide: true,
      infinite: false,
      touchThreshold: 100,
      speed: 100,
    });
  }
  var internalPage = $('.homepage-wrapper').length > 0 ? true: false;
  if(internalPage) {
    $('#fullpage').fullpage({
      sectionSelector: '.snap',
      slideSelector: '.ignore',
      scrollOverflow: true,
      paddingTop: mediaQuery.matches ? 80 : 0,
      fixedElements: 'header',
      anchors: ['landing','home', 'story', 'partners', 'form'],
      afterLoad: function(anchorLink, index){
        if(index === 1){
          if (history.pushState) {
            window.history.replaceState('', document.title, window.location.pathname);
          }
        }
        switch(anchorLink) {
           case 'landing':
            $('.half-logo').addClass('hide');
            $('.small-logo').addClass('hide');
            $('.small-blue-logo').addClass('hide');
            $('.nav-wrapper').addClass('hide');
            $('.story-slider').slick('slickPause');
            break;
        }
      },
      onLeave: function(index, nextIndex, direction){
        switch(nextIndex) {
          case 2:
            $('.half-logo').removeClass('hide');
            $('.small-logo').removeClass('hide');
            $('.small-blue-logo').addClass('hide');
            $('.nav-wrapper').removeClass('hide');
            $('.nav-wrapper').removeClass('blue');
            $('.story-slider').slick('slickPause');
            break;
          case 3:
            $('.half-logo').addClass('hide');
            $('.small-logo').removeClass('hide');
            $('.small-blue-logo').addClass('hide');
            $('.nav-wrapper').removeClass('hide');
            $('.nav-wrapper').removeClass('blue');
            $('.story-slider').slick('slickPlay');
            break;
          case 4:
            $('.half-logo').addClass('hide');
            $('.small-logo').addClass('hide');
            $('.small-blue-logo').removeClass('hide');
            $('.nav-wrapper').removeClass('hide');
            $('.nav-wrapper').addClass('blue');
            $('.story-slider').slick('slickPause');
            break;
          case 5:
            $('.half-logo').addClass('hide');
            $('.small-logo').addClass('hide');
            $('.small-blue-logo').removeClass('hide');
            $('.nav-wrapper').removeClass('hide');
            $('.nav-wrapper').addClass('blue');
            $('.story-slider').slick('slickPause');
            break;
          case 1:
            $('.landing').hide();
            // $('.half-logo').addClass('hide');
            // $('.small-logo').addClass('hide');
            // $('.small-blue-logo').addClass('hide');
            // $('.nav-wrapper').addClass('hide');
            // $('.story-slider').slick('slickPause');
            break;
        }
      },
    });
  }
});
var typingOptions = function(string) {
  return {
    startDelay: 100,
    typeSpeed: 250,
    strings: [string],
    showCursor: false,
    fadeOut: false,
    fadeOutClass: 'typed-fade-out',
    fadeOutDelay: 500,
    onComplete: function() {
      setTimeout(function() {
        $('.home-slider').slick('slickNext');
      }, 2000)
    },
  }
}
$('.home-slider').on('init', function(){
  var options = typingOptions('Culture');
  $('.typed').empty();
  var typed = new Typed('.typed', options);
});
$('.home-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
  if(currentSlide === 0){
    var options = typingOptions('Home');
    $('.typed-2').empty();
    var typedSeconed = new Typed('.typed-2', options);
  } else if(currentSlide === 1) {
    var options = typingOptions('Family');
    $('.typed-3').empty();
    var typedThird = new Typed('.typed-3', options);
  } else if(currentSlide === 2) {
    var options = typingOptions('Extravagant');
    $('.typed-4').empty();
    var typedFourth = new Typed('.typed-4', options);
  } else if(currentSlide === 3) {
    var options = typingOptions('Epic');
    $('.typed-5').empty();
    var typedFifth = new Typed('.typed-5', options);
  } else if(nextSlide === 0) {
    var options = typingOptions('Culture');
    $('.typed').empty();
    var typed = new Typed('.typed', options);
  }
});
